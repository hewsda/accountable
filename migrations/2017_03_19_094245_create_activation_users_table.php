<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActivationUsersTable extends Migration
{

    public function up()
    {
        Schema::create('activation_users', function (Blueprint $table) {
            $table->uuid('id');
            $table->string('user_id', 36)->unique();
            $table->string('token', 150);

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('activation_users');
    }
}
