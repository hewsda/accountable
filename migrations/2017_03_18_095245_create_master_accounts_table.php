<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMasterAccountsTable extends Migration
{

    public function up()
    {
        Schema::create('master_accounts', function (Blueprint $table) {
            $table->uuid('id');
            $table->string('account_id', 36);
            $table->string('account_type_id', 36);
            $table->string('account_type_table', 150);
        });
    }

    public function down()
    {
        Schema::dropIfExists('master_accounts');
    }
}