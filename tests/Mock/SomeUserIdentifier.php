<?php

declare(strict_types=1);

namespace AccountableTests\Mock;

class SomeUserIdentifier
{
    public function __toString(): string
    {
        return '3361671b-f347-479b-bf98-a0219d7d15e2';
    }
}