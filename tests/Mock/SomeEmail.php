<?php

declare(strict_types=1);

namespace AccountableTests\Mock;

class SomeEmail
{
    public function __toString(): string
    {
        return 'some@email.com';
    }
}