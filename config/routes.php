<?php

return [
    'accountable' => [
        'service_bus' => [

            /**
             * Command bus
             */
            'command_bus' => [
                'plugins' => [
                    \Hewsda\Commander\Plugin\ServiceLocatorPlugin::class
                ],
                'router' => [
                    'routes' => [

                        /**
                         * Registration
                         */
                        \Hewsda\Accountable\Account\Model\LocalUser\UseCases\LocalAccountSignUp::class => \Hewsda\Accountable\Account\Model\LocalUser\Handlers\LocalUserSignUpHandler::class,
                        \Hewsda\Accountable\Account\Model\LocalUser\UseCases\ChangeLocalPassword::class => \Hewsda\Accountable\Account\Model\LocalUser\Handlers\ChangeLocalPasswordHandler::class,

                    ]
                ]
            ],

            /**
             * Event bus
             */
            'event_bus' => [
                'plugins' => [
                    \Hewsda\Commander\Plugin\ServiceLocatorPlugin::class,
                    \Hewsda\Commander\Plugin\OnEventStrategy::class,
                    //\Hewsda\Commander\Plugin\OnEventNameStrategy::class
                ],
                'router' => [
                    'routes' => [
                        \Hewsda\Accountable\Account\Event\AccountWasCreated::class => [
                            \Hewsda\Accountable\Projection\Account\AccountProjector::class,
                            \Hewsda\Accountable\Projection\Account\AccountMasterProjector::class,
                        ],

                        \Hewsda\Accountable\Account\Event\LocalAccountWasCreated::class => [
                            \Hewsda\Accountable\Account\Model\LocalUser\Handlers\LocalUserCreator::class
                        ],

                        \Hewsda\Accountable\Account\Model\LocalUser\Events\LocalUserWasRegistered::class => [
                            \Hewsda\Accountable\Projection\Account\LocalUserProjector::class
                        ],
                        \Hewsda\Accountable\Account\Model\LocalUser\Events\LocalPasswordWasChanged::class => [
                            \Hewsda\Accountable\Projection\Account\LocalUserProjector::class
                        ]
                    ]
                ]
            ],

            /**
             * Query bus
             */
            'query_bus' => [
                'plugins' => [
                    \Hewsda\Commander\Plugin\ServiceLocatorPlugin::class
                ],
                'router' => [
                    'routes' => [

                    ]
                ]
            ]

        ]
    ]
];