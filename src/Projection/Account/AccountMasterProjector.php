<?php

declare(strict_types=1);

namespace Hewsda\Accountable\Projection\Account;

class AccountMasterProjector
{
    public function onEvent($event)
    {
        logger(get_class($event));
    }
}