<?php

declare(strict_types=1);

namespace Hewsda\Accountable\Projection\Account;

class AccountProjector
{
    public function onEvent($event)
    {
        logger(get_class($event));
    }
}