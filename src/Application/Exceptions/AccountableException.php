<?php

declare(strict_types=1);

namespace Hewsda\Accountable\Application\Exceptions;

class AccountableException extends \RuntimeException
{

}