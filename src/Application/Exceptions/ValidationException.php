<?php

declare(strict_types=1);

namespace Hewsda\Accountable\Application\Exceptions;

use Assert\InvalidArgumentException;

class ValidationException extends InvalidArgumentException
{
    // need to wrap to accountable exception
}