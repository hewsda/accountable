<?php

declare(strict_types=1);

namespace Hewsda\Accountable\Application\Exceptions;

use Assert\Assertion as BaseAssertion;

class Assertion extends BaseAssertion
{
    protected static $exceptionClass = ValidationException::class;
}