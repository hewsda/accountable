<?php

declare(strict_types=1);

namespace Hewsda\Accountable\Application\Providers;

use Hewsda\Accountable\Account\Account;
use Hewsda\Accountable\Account\Model\LocalUser\LocalUser;
use Hewsda\Accountable\Account\Model\LocalUser\Repository\LocalAccountCollection;
use Hewsda\Accountable\Account\Repository\AccountCollection;
use Hewsda\Accountable\Infrastructure\Repository\EventStoreAccount;
use Hewsda\Accountable\Infrastructure\Repository\EventStoreLocalAccount;
use Hewsda\Accountable\NotAEventStore\Aggregate\AggregateType;
use Hewsda\Accountable\NotAEventStore\CacheAdapter\EventStoreCacheAdapter;
use Hewsda\Accountable\NotAEventStore\EventStore;
use Hewsda\Accountable\NotAEventStore\Plugin\EventPublisher;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\ServiceProvider;
use Prooph\Common\Event\ProophActionEventEmitter;

class EventStoreServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->registerEventStorePlugin();
    }

    public function register()
    {
        $this->registerEventStore();

        $this->registerAccountCollection();

        $this->registerLocalAccountCollection();
    }

    private function registerEventStore()
    {
        // base on env config
        $this->app->singleton(EventStore::class, function (Application $app) {
            return new EventStore(
                $app->make(EventStoreCacheAdapter::class),
                new ProophActionEventEmitter()
            );
        });
    }

    private function registerAccountCollection()
    {
        $this->app->bind(EventStoreAccount::class, function (Application $app) {
            return new EventStoreAccount(
                $app->make(EventStore::class),
                AggregateType::fromAggregateRootClass(Account::class)
            );
        });

        $this->app->alias(EventStoreAccount::class, AccountCollection::class);
    }

    private function registerLocalAccountCollection()
    {
        $this->app->bind(EventStoreLocalAccount::class, function (Application $app) {
            return new EventStoreLocalAccount(
                $app->make(EventStore::class),
                AggregateType::fromAggregateRootClass(LocalUser::class)
            );
        });

        $this->app->alias(EventStoreLocalAccount::class, LocalAccountCollection::class);
    }

    private function registerEventStorePlugin()
    {
        $this->app->bind(EventPublisher::class);

        $this->app[EventPublisher::class]->setUp($this->app[EventStore::class]);
    }

    public function provides()
    {
        // checkMe
        return [EventPublisher::class];
    }
}