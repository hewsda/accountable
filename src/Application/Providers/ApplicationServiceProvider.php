<?php

declare(strict_types=1);

namespace Hewsda\Accountable\Application\Providers;

use Illuminate\Support\ServiceProvider;

class ApplicationServiceProvider extends ServiceProvider
{

    public function boot()
    {
        /**
         * Config, migrations
         */
        $this->app->register(ConfigurationServiceProvider::class);

        /**
         * Domain services
         */
        $this->app->register(DomainServiceProvider::class);
    }

    public function register()
    {
        /**
         * Accountable Commander Bus
         */
        $this->app->register(CommanderServiceProvider::class);

        /**
         * Accountable Router and routes
         */
        $this->app->register(RouterServiceProvider::class);

        /**
         * Projection Finder
         */
        $this->app->register(ProjectorServiceProvider::class);

        /**
         * EventStore Collections
         * todo split collection from event store
         */
        $this->app->register(EventStoreServiceProvider::class);
    }
}