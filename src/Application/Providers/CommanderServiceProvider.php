<?php

declare(strict_types=1);

namespace Hewsda\Accountable\Application\Providers;

use Hewsda\Commander\Providers\CommanderServiceProvider as CommanderBase;

class CommanderServiceProvider extends CommanderBase
{
    protected $namespace = 'accountable';

    public function boot()
    {
        // todo publish config
        parent::boot();
    }

    protected function registerConfig()
    {
        $this->mergeConfigFrom(__DIR__ . '/../../../config/routes.php', 'commander');
    }
}