<?php

declare(strict_types=1);

namespace Hewsda\Accountable\Application\Providers;

use Hewsda\Accountable\Account\Model\LocalUser\Service\CheckLocalUniqueEmailAddress;
use Hewsda\Accountable\Account\Model\LocalUser\Service\EncoderPasswordService;
use Hewsda\Accountable\Account\Service\CheckUniqueEmailAddress;
use Hewsda\Accountable\Infrastructure\Service\Credential\LocalPasswordBCryptEncoder;
use Hewsda\Accountable\Infrastructure\Service\EmailAdress\CheckUniqueEmailAddressAccount;
use Hewsda\Accountable\Infrastructure\Service\EmailAdress\CheckUniqueEmailAddressLocalAccount;
use Illuminate\Support\ServiceProvider;

class DomainServiceProvider extends ServiceProvider
{
    /**
     * @var array
     */
    protected $services = [
        EncoderPasswordService::class => LocalPasswordBCryptEncoder::class,
        CheckUniqueEmailAddress::class => CheckUniqueEmailAddressAccount::class,
        CheckLocalUniqueEmailAddress::class => CheckUniqueEmailAddressLocalAccount::class
    ];

    public function boot()
    {
        foreach ($this->services as $abstract => $concrete) {
            $this->app->bind($abstract, $concrete);
        }
    }
}