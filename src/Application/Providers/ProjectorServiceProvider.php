<?php

declare(strict_types=1);

namespace Hewsda\Accountable\Application\Providers;

use Hewsda\Accountable\Projection\Account\AccountFinder;
use Hewsda\Accountable\Projection\Account\LocalUserFinder;
use Illuminate\Support\ServiceProvider;

class ProjectorServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(AccountFinder::class);
        $this->app->bind(LocalUserFinder::class);
    }
}