<?php

declare(strict_types=1);

namespace Hewsda\Accountable\Application\Providers;

use Hewsda\Commander\Providers\RouterServiceProvider as CommanderRouterServiceProvider;

class RouterServiceProvider extends CommanderRouterServiceProvider
{
    protected $namespace = 'accountable';
}