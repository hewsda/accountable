<?php

declare(strict_types=1);

namespace Hewsda\Accountable\Account\Contracts;

interface Value
{
    public function sameValueAs(Value $aValue): bool;
}