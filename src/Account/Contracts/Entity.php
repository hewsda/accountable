<?php

declare(strict_types=1);

namespace Hewsda\Accountable\Account\Contracts;

interface Entity
{
    public function sameIdentityAs(Entity $aEntity): bool;
}