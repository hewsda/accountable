<?php

declare(strict_types=1);

namespace Hewsda\Accountable\Account\Contracts;

interface AccountTypeIdentifier extends Value
{
    public function getIdentifier(): AccountTypeIdentifier;

    public static function fromIdentifier($identifier): AccountTypeIdentifier;

    public function toString(): string;
}