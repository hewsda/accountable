<?php

declare(strict_types=1);

namespace Hewsda\Accountable\Account\Contracts;

use Hewsda\Accountable\Account\Values\AccountId;
use Hewsda\Accountable\Account\Values\EmailAddress;

interface AccountInterface extends Entity
{
    public function getAccountId(): AccountId;

    public function getEmail(): EmailAddress;
}