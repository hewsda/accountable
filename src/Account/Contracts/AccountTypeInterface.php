<?php

declare(strict_types=1);

namespace Hewsda\Accountable\Account\Contracts;

use Hewsda\Accountable\Account\Values\AccountIdentifier;

interface AccountTypeInterface extends AccountInterface
{
    public function getIdentifier(): AccountIdentifier;
}