<?php

declare(strict_types=1);

namespace Hewsda\Accountable\Account;

use Hewsda\Accountable\Account\Values\AccountMaster;
use Hewsda\Accountable\Account\Values\AccountSecondary;
use Illuminate\Support\Collection;

class AccountHolder
{
    /**
     * @var Collection
     */
    private $collection;

    /**
     * @var AccountMaster
     */
    private $master;

    /**
     * AccountHolder constructor.
     *
     * @param AccountMaster $master
     */
    public function __construct(AccountMaster $master)
    {
        $this->master = $master;
        $this->collection = new Collection();
    }

    public function attach(AccountSecondary $secondary): AccountHolder
    {
        if ($this->collection->contains($secondary)) {
            throw new \RuntimeException('Account already exists.');
        }

        if ($secondary->isLocal()) {
            if (!$this->hasLocal()) {
                return $this->addSecondary($secondary);
            }

            throw new \InvalidArgumentException('Local account already exists.');
        }

        throw new \InvalidArgumentException('Account type unknown.');
    }

    public function replaceAndTransformMasterToSecondaryWith(AccountMaster $replace)
    {
        if ($this->master->sameValueAs($replace)) {
            throw new \InvalidArgumentException('Account master can not be replaced with itself.');
        }

        $toSecondary = AccountSecondary::fromValues(
            $this->master->getAccountIdentifier(),
            $this->master->getUuid()
        );
    }

    protected function addSecondary(AccountSecondary $secondary): AccountHolder
    {
        $this->collection->push($secondary);

        return $this;
    }

    public function hasLocal(): bool
    {
        return $this->master->isLocal() ||
            $this->collection->filter(function (AccountSecondary $secondary) {
                return $secondary->isLocal();
            })->isNotEmpty();
    }
}