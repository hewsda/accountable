<?php

declare(strict_types=1);

namespace Hewsda\Accountable\Account\Service;

use Hewsda\Accountable\Account\Values\EmailAddress;

interface CheckUniqueEmailAddress
{
    public function __invoke(EmailAddress $emailAddress): bool;
}