<?php

declare(strict_types=1);

namespace Hewsda\Accountable\Account\Model\LocalUser\Values;

use Hewsda\Accountable\Account\Contracts\Value;
use Hewsda\Accountable\Application\Exceptions\Assertion;

class Password implements Value
{
    const MIN_LENGTH = 8;
    const MAX_LENGTH = 250;

    /**
     * @var string
     */
    private $password;

    /**
     * Password constructor.
     *
     * @param string $password
     */
    private function __construct(string $password)
    {
        $this->password = $password;
    }

    public static function fromString($password, $passwordConfirmation): self
    {
        Assertion::string($password);
        Assertion::string($passwordConfirmation);

        Assertion::same($password, $passwordConfirmation);
        Assertion::betweenLength($passwordConfirmation, self::MIN_LENGTH, self::MAX_LENGTH);

        return new self($passwordConfirmation);
    }

    public function sameValueAs(Value $aValue): bool
    {
        return $aValue instanceof $this &&
            $this->toString() === $aValue->toString();
    }

    public function toString(): string
    {
        return $this->password;
    }
}