<?php

declare(strict_types=1);

namespace Hewsda\Accountable\Account\Model\LocalUser\Values;

use Hewsda\Accountable\Account\Contracts\Value;
use Hewsda\Accountable\Application\Exceptions\Assertion;
use Hewsda\Accountable\Application\Exceptions\ValidationException;

class BCryptEncodedPassword extends EncodedPassword
{
    /**
     * @var string
     */
    private $password;

    /**
     * EncodedPassword constructor.
     *
     * @param string $password
     */
    private function __construct(string $password)
    {
        $this->password = $password;
    }

    public static function fromPassword(Password $password): self
    {
        return new static(bcrypt($password->toString()));
    }

    public static function fromEncodedPassword($password): self
    {
        Assertion::string($password);

        $info = password_get_info($password);

        ['algo' => $algo, 'algoName' => $name, 'options' => $options] = $info;

        if (0 === $algo || 'unknown' === $name || !is_array($options)) {

            throw new ValidationException('Credentials not valid', Assertion::INVALID_STRING, EncodedPassword::class, '*****');
        }

        return new static($password);
    }

    public function toString(): string
    {
        return $this->password;
    }

    public function sameValueAs(Value $aValue): bool
    {
        return $aValue instanceof $this && $this->toString() === $aValue->toString();
    }

    public function isValid(string $clearPassword): bool
    {
        return password_verify($clearPassword, $this->toString());
    }
}