<?php

declare(strict_types=1);

namespace Hewsda\Accountable\Account\Model\LocalUser\Values;

use Hewsda\Accountable\Account\Contracts\Value;

abstract class EncodedPassword implements Value
{
    // fixMe should be an interface
}