<?php

declare(strict_types=1);

namespace Hewsda\Accountable\Account\Model\LocalUser\Values;

use Hewsda\Accountable\Account\Contracts\AccountTypeIdentifier;
use Hewsda\Accountable\Account\Contracts\Value;
use Hewsda\Accountable\Application\Exceptions\Assertion;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class LocalUserId implements AccountTypeIdentifier
{
    /**
     * @var UuidInterface
     */
    private $uid;

    /**
     * LocalUserId constructor.
     *
     * @param UuidInterface $uid
     */
    private function __construct(UuidInterface $uid)
    {
        $this->uid = $uid;
    }

    public static function nextIdentity(): self
    {
        return new self(Uuid::uuid4());
    }

    public static function fromIdentifier($identifier): AccountTypeIdentifier
    {
        return self::fromString($identifier);
    }

    public static function fromString($userId): self
    {
        Assertion::string($userId);
        Assertion::uuid($userId);

        return new self(Uuid::fromString($userId));
    }

    public function toString(): string
    {
        return $this->uid->toString();
    }

    public function sameValueAs(Value $aValue): bool
    {
        return $aValue instanceof $this && $this->toString() === $aValue->toString();
    }

    public function __toString(): string
    {
        return $this->toString();
    }

    public function getIdentifier(): AccountTypeIdentifier
    {
        return $this;
    }
}