<?php

declare(strict_types=1);

namespace Hewsda\Accountable\Account\Model\LocalUser\UseCases;

use Hewsda\Accountable\Account\Contracts\AccountTypeIdentifier;
use Hewsda\Accountable\Account\Model\LocalUser\Values\LocalUserId;
use Hewsda\Accountable\Account\Model\LocalUser\Values\Password;
use Hewsda\Accountable\Account\UseCases\AccountSignUp;

class LocalAccountSignUp extends AccountSignUp
{
    public static function withData($accountId, $localUserId, $email, $password, $passwordConfirmation): self
    {
        return new self([
            'account_id' => $accountId,
            'local_user_id' => $localUserId,
            'email' => $email,
            'password' => $password,
            'password_confirmation' => $passwordConfirmation
        ]);
    }

    public function getAccountTypeIdentifier(): AccountTypeIdentifier
    {
        return LocalUserId::fromString($this->payload['local_user_id']);
    }

    public function getPassword(): Password
    {
        return Password::fromString($this->payload['password'], $this->payload['password_confirmation']);
    }
}