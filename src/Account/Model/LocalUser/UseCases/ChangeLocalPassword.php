<?php

declare(strict_types=1);

namespace Hewsda\Accountable\Account\Model\LocalUser\UseCases;

use Hewsda\Accountable\Account\Model\LocalUser\Values\LocalUserId;
use Hewsda\Accountable\Account\Model\LocalUser\Values\Password;
use Hewsda\Accountable\Account\Values\AccountId;
use Prooph\Common\Messaging\Command;
use Prooph\Common\Messaging\PayloadConstructable;
use Prooph\Common\Messaging\PayloadTrait;

class ChangeLocalPassword extends Command implements PayloadConstructable
{
    use PayloadTrait;

    public static function withData($accountId, $localUserId, $actualPassword, $newPassword, $newPasswordConfirmation)
    {
        return new self([
            'account_id' => $accountId,
            'local_user_id' => $localUserId,
            'actual_password' => $actualPassword,
            'new_password' => $newPassword,
            'new_password_confirmation' => $newPasswordConfirmation
        ]);
    }

    public function getAccountId(): AccountId
    {
        return AccountId::fromString($this->payload['account_id']);
    }

    public function getLocalUserId(): LocalUserId
    {
        return LocalUserId::fromString($this->payload['local_user_id']);
    }

    public function getActualPassword(): Password
    {
        return Password::fromString($this->payload['actual_password'], $this->payload['actual_password']);
    }

    public function getNewPassword(): Password
    {
        return Password::fromString($this->payload['new_password'], $this->payload['new_password_confirmation']);
    }
}