<?php

declare(strict_types=1);

namespace Hewsda\Accountable\Account\Model\LocalUser\Service;

use Hewsda\Accountable\Account\Model\LocalUser\Values\EncodedPassword;
use Hewsda\Accountable\Account\Model\LocalUser\Values\Password;
use Hewsda\Accountable\Account\Values\AccountIdentifier;

class LocalPasswordChanger
{
    /**
     * @var AccountIdentifier
     */
    private $identifier;

    /**
     * @var Password
     */
    private $actualPassword;

    /**
     * @var EncodedPassword
     */
    private $newPassword;

    /**
     * LocalPasswordChanger constructor.
     *
     * @param AccountIdentifier $identifier
     * @param Password $actualPassword
     * @param EncodedPassword $newPassword
     */
    public function __construct(AccountIdentifier $identifier, Password $actualPassword, EncodedPassword $newPassword)
    {
        $this->identifier = $identifier;
        $this->actualPassword = $actualPassword;
        $this->newPassword = $newPassword;
    }

    public function validate(EncodedPassword $current): ?EncodedPassword
    {
        if (!$this->isActualPasswordValid($current)) {
            throw new \RuntimeException('Actual password is not valid.');
        }

        if ($this->isActualPasswordSameAsCurrent($current)) {
            return null;
        }

        return $this->newPassword;
    }

    protected function isActualPasswordValid(EncodedPassword $current): bool
    {
        return $current->isValid($this->actualPassword->toString());
    }

    protected function isActualPasswordSameAsCurrent(EncodedPassword $current): bool
    {
        return $current->toString() === $this->newPassword->toString() ||
            $this->newPassword->isValid($this->actualPassword->toString());
    }

    public function getIdentifier(): AccountIdentifier
    {
        return $this->identifier;
    }
}