<?php

declare(strict_types=1);

namespace Hewsda\Accountable\Account\Model\LocalUser\Service;

use Hewsda\Accountable\Account\Service\CheckUniqueEmailAddress;
use Hewsda\Accountable\Account\Values\EmailAddress;

interface CheckLocalUniqueEmailAddress extends CheckUniqueEmailAddress
{
    public function invokeForAccountType(EmailAddress $emailAddress): bool;
}