<?php

declare(strict_types=1);

namespace Hewsda\Accountable\Account\Model\LocalUser\Service;

use Hewsda\Accountable\Account\Model\LocalUser\Values\EncodedPassword;
use Hewsda\Accountable\Account\Model\LocalUser\Values\Password;

interface EncoderPasswordService
{
    public function __invoke(Password $password): EncodedPassword;
}