<?php

declare(strict_types=1);

namespace Hewsda\Accountable\Account\Model\LocalUser\Handlers;

use Hewsda\Accountable\Account\Model\LocalUser\Repository\LocalAccountCollection;
use Hewsda\Accountable\Account\Model\LocalUser\Service\EncoderPasswordService;
use Hewsda\Accountable\Account\Model\LocalUser\Service\LocalPasswordChanger;
use Hewsda\Accountable\Account\Model\LocalUser\UseCases\ChangeLocalPassword;
use Hewsda\Accountable\Account\Repository\AccountCollection;
use Hewsda\Accountable\Account\Values\AccountIdentifier;

class ChangeLocalPasswordHandler
{
    /**
     * @var AccountCollection
     */
    private $accountStore;

    /**
     * @var LocalAccountCollection
     */
    private $localStore;

    /**
     * @var EncoderPasswordService
     */
    private $encoderPassword;

    /**
     * ChangeLocalPasswordHandler constructor.
     *
     * @param AccountCollection $accountStore
     * @param LocalAccountCollection $localStore
     * @param EncoderPasswordService $encoderPassword
     */
    public function __construct(AccountCollection $accountStore,
                                LocalAccountCollection $localStore,
                                EncoderPasswordService $encoderPassword)
    {
        $this->accountStore = $accountStore;
        $this->localStore = $localStore;
        $this->encoderPassword = $encoderPassword;
    }

    public function __invoke(ChangeLocalPassword $command): void
    {
        [$account, $local] = $this->requireEntity($command);

        $changed = $account->changeLocalPassword($local, $this->newPasswordChanger($command));

        $changed and $this->localStore->save($changed);
    }

    private function newPasswordChanger(ChangeLocalPassword $command): LocalPasswordChanger
    {
        $encodedPassword = ($this->encoderPassword)($command->getNewPassword());

        $identifier = AccountIdentifier::fromValues($command->getAccountId(), $command->getLocalUserId());

        return new LocalPasswordChanger($identifier, $command->getActualPassword(), $encodedPassword);
    }

    private function requireEntity(ChangeLocalPassword $command): array
    {
        $account = $this->accountStore->get($command->getAccountId());

        if (!$account) {
            throw new \RuntimeException('Account is unknown.');
        }

        $local = $this->localStore->get($command->getLocalUserId());

        if (!$local) {
            throw new \RuntimeException('Account is unknown.');
        }

        return [$account, $local];
    }
}