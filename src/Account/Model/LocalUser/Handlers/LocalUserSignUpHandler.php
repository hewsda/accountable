<?php

declare(strict_types=1);

namespace Hewsda\Accountable\Account\Model\LocalUser\Handlers;

use Hewsda\Accountable\Account\Account;
use Hewsda\Accountable\Account\Model\LocalUser\Repository\LocalAccountCollection;
use Hewsda\Accountable\Account\Model\LocalUser\Service\CheckLocalUniqueEmailAddress;
use Hewsda\Accountable\Account\Model\LocalUser\Service\EncoderPasswordService;
use Hewsda\Accountable\Account\Model\LocalUser\UseCases\LocalAccountSignUp;
use Hewsda\Accountable\Account\Repository\AccountCollection;
use Hewsda\Accountable\Account\Service\CheckUniqueEmailAddress;

class LocalUserSignUpHandler
{
    /**
     * @var EncoderPasswordService
     */
    private $encoderPassword;

    /**
     * @var CheckLocalUniqueEmailAddress
     */
    private $checkUniqueLocalEmail;

    /**
     * @var AccountCollection
     */
    private $accountStore;

    /**
     * @var LocalAccountCollection
     */
    private $localStore;

    /**
     * @var CheckUniqueEmailAddress
     */
    private $checkUniqueAccountEmail;

    /**
     * LocalUserSignUpHandler constructor.
     *
     * @param AccountCollection $accountStore
     * @param LocalAccountCollection $localStore
     * @param EncoderPasswordService $encoderPassword
     * @param CheckUniqueEmailAddress $checkUniqueAccountEmail
     * @param CheckLocalUniqueEmailAddress $checkUniqueLocalEmail
     */
    public function __construct(AccountCollection $accountStore,
                                LocalAccountCollection $localStore,
                                EncoderPasswordService $encoderPassword,
                                CheckUniqueEmailAddress $checkUniqueAccountEmail,
                                CheckLocalUniqueEmailAddress $checkUniqueLocalEmail)
    {
        $this->accountStore = $accountStore;
        $this->localStore = $localStore;
        $this->encoderPassword = $encoderPassword;
        $this->checkUniqueAccountEmail = $checkUniqueAccountEmail;
        $this->checkUniqueLocalEmail = $checkUniqueLocalEmail;
    }

    public function __invoke(LocalAccountSignUp $signUp)
    {
        // todo account

        $local = $this->localStore->get($signUp->getAccountTypeIdentifier());

        if ($local) {
            throw new \RuntimeException('Local user already exists.');
        }

        if (!($this->checkUniqueLocalEmail)($signUp->getEmail())) {
            throw new \RuntimeException('Local email already exists.');
        }

        $encodedPassword = ($this->encoderPassword)($signUp->getPassword());

        $user = Account::createLocalUser(
            $signUp->getIdentifier(),
            $signUp->getEmail(),
            $encodedPassword
        );

        $this->accountStore->save($user);
    }
}