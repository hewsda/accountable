<?php

declare(strict_types=1);

namespace Hewsda\Accountable\Account\Model\LocalUser\Handlers;

use Hewsda\Accountable\Account\Event\LocalAccountWasCreated;
use Hewsda\Accountable\Account\Model\LocalUser\LocalUser;
use Hewsda\Accountable\Account\Model\LocalUser\Repository\LocalAccountCollection;
use Hewsda\Accountable\Account\Repository\AccountCollection;

class LocalUserCreator
{
    /**
     * @var AccountCollection
     */
    private $accountStore;

    /**
     * @var LocalAccountCollection
     */
    private $localStore;

    /**
     * LocalUserCreator constructor.
     *
     * @param AccountCollection $accountStore
     * @param LocalAccountCollection $localStore
     */
    public function __construct(AccountCollection $accountStore, LocalAccountCollection $localStore)
    {
        $this->accountStore = $accountStore;
        $this->localStore = $localStore;
    }

    public function __invoke(LocalAccountWasCreated $event)
    {
        $account = $this->accountStore->get($event->getAccountMaster()->getAccountIdentifier()->getAccountId());

        if (!$account) {
            throw new \RuntimeException('Account does not exists.');
        }

        $local = LocalUser::signUp(
            $event->getAccountMaster()->getAccountIdentifier(),
            $event->getEmailAddress(),
            $event->getPassword());

        $this->localStore->save($local);
    }
}