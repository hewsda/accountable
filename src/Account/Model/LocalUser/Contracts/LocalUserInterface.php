<?php

declare(strict_types=1);

namespace Hewsda\Accountable\Account\Model\LocalUser\Contracts;

use Hewsda\Accountable\Account\Contracts\AccountTypeIdentifier;
use Hewsda\Accountable\Account\Contracts\AccountTypeInterface;
use Hewsda\Accountable\Account\Model\LocalUser\Values\EncodedPassword;

interface LocalUserInterface extends AccountTypeInterface
{
    public function getAccountTypeIdentifier(): AccountTypeIdentifier;

    public function getPassword(): EncodedPassword;
}