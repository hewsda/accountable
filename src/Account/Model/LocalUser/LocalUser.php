<?php

declare(strict_types=1);

namespace Hewsda\Accountable\Account\Model\LocalUser;

use Hewsda\Accountable\Account\AccountType;
use Hewsda\Accountable\Account\Model\LocalUser\Contracts\LocalUserInterface;
use Hewsda\Accountable\Account\Model\LocalUser\Events\LocalPasswordWasChanged;
use Hewsda\Accountable\Account\Model\LocalUser\Events\LocalUserWasRegistered;
use Hewsda\Accountable\Account\Model\LocalUser\Service\LocalPasswordChanger;
use Hewsda\Accountable\Account\Model\LocalUser\Values\EncodedPassword;
use Hewsda\Accountable\Account\Values\AccountIdentifier;
use Hewsda\Accountable\Account\Values\EmailAddress;

class LocalUser extends AccountType implements LocalUserInterface
{
    /**
     * @var EncodedPassword
     */
    private $password;

    public static function signUp(AccountIdentifier $identifier, EmailAddress $emailAddress, EncodedPassword $password): self
    {
        $self = new self();

        $self->publish(LocalUserWasRegistered::withData($identifier, $emailAddress, $password));

        return $self;
    }

    public function changePassword(LocalPasswordChanger $changer): ?LocalUser
    {
        $newEncodedPassword = $changer->validate($this->password);

        if ($newEncodedPassword) {
            $this->publish(LocalPasswordWasChanged::withData($changer->getIdentifier(), $newEncodedPassword));

            return $this;
        }

        return null;
    }

    public function whenLocalUserWasRegistered(LocalUserWasRegistered $event): void
    {
        $this->accountId = $event->getAccountIdentifier()->getAccountId();
        $this->typeIdentifier = $event->getAccountIdentifier()->getTypeIdentifier();
        $this->email = $event->getEmail();
        $this->password = $event->getPassword();
    }

    public function whenLocalPasswordWasChanged(LocalPasswordWasChanged $event)
    {
        $this->password = $event->getPassword();
    }

    public function getPassword(): EncodedPassword
    {
        return $this->password;
    }
}