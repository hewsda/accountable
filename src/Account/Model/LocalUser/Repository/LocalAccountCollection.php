<?php

declare(strict_types=1);

namespace Hewsda\Accountable\Account\Model\LocalUser\Repository;

use Hewsda\Accountable\Account\Model\LocalUser\LocalUser;
use Hewsda\Accountable\Account\Model\LocalUser\Values\LocalUserId;
use Hewsda\Accountable\Account\Repository\AccountTypeCollection;

interface LocalAccountCollection extends AccountTypeCollection
{
    public function get(LocalUserId $localUserId): ?LocalUser;

    public function save(LocalUser $localUser): void;
}