<?php

declare(strict_types=1);

namespace Hewsda\Accountable\Account\Model\LocalUser\Events;

use Hewsda\Accountable\Account\Model\LocalUser\Values\BCryptEncodedPassword;
use Hewsda\Accountable\Account\Model\LocalUser\Values\EncodedPassword;
use Hewsda\Accountable\Account\Model\LocalUser\Values\LocalUserId;
use Hewsda\Accountable\Account\Values\AccountId;
use Hewsda\Accountable\Account\Values\AccountIdentifier;
use Hewsda\EventSourcing\AggregateChanged;

class LocalPasswordWasChanged extends AggregateChanged
{
    /**
     * @var AccountId
     */
    private $accountId;

    /**
     * @var EncodedPassword
     */
    private $password;

    public static function withData(AccountIdentifier $identifier, EncodedPassword $password): self
    {
        $self = self::occur($identifier->getTypeIdentifier()->toString(), [
            'account_id' => $identifier->getAccountId()->toString(),
            'password' => $password->toString()
        ]);

        $self->accountId = $identifier->getAccountId();
        $self->password = $password;

        return $self;
    }

    public function getAccountIdentifier(): AccountIdentifier
    {
        return AccountIdentifier::fromValues(
            AccountId::fromString($this->payload['account_id']),
            LocalUserId::fromString($this->aggregateId())
        );
    }

    public function getPassword(): EncodedPassword
    {
        return $this->password ?? BCryptEncodedPassword::fromEncodedPassword($this->payload['password']);
    }

}