<?php

declare(strict_types=1);

namespace Hewsda\Accountable\Account\Model\LocalUser\Events;

use Hewsda\Accountable\Account\Model\LocalUser\Values\BCryptEncodedPassword;
use Hewsda\Accountable\Account\Model\LocalUser\Values\EncodedPassword;
use Hewsda\Accountable\Account\Model\LocalUser\Values\LocalUserId;
use Hewsda\Accountable\Account\Values\AccountId;
use Hewsda\Accountable\Account\Values\AccountIdentifier;
use Hewsda\Accountable\Account\Values\EmailAddress;
use Hewsda\EventSourcing\AggregateChanged;

class LocalUserWasRegistered extends AggregateChanged
{

    /**
     * @var AccountId
     */
    private $accountId;

    /**
     * @var EncodedPassword
     */
    private $password;

    /**
     * @var EmailAddress
     */
    private $email;

    public static function withData(AccountIdentifier $accountIdentifier, EmailAddress $email, EncodedPassword $password): self
    {
        $self = self::occur($accountIdentifier->getTypeIdentifier()->toString(),
            [
                'account_id' => $accountIdentifier->getAccountId()->toString(),
                'email' => $email->toString(),
                'password' => $password->toString()
            ]);

        $self->accountId = $accountIdentifier->getAccountId();
        $self->email = $email;
        $self->password = $password;

        return $self;
    }

    public function getAccountIdentifier(): AccountIdentifier
    {
        return AccountIdentifier::fromValues(
            $this->accountId ?? AccountId::fromString($this->payload['account_id']),
            LocalUserId::fromString($this->aggregateId())
        );
    }

    public function getPassword(): EncodedPassword
    {
        return $this->password ?? BCryptEncodedPassword::fromEncodedPassword($this->payload['password']);
    }

    public function getEmail(): EmailAddress
    {
        return $this->email ?? EmailAddress::fromString($this->payload['email']);
    }
}