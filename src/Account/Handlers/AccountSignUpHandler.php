<?php

declare(strict_types=1);

namespace Hewsda\Accountable\Account\Handlers;

use Hewsda\Accountable\Account\Account;
use Hewsda\Accountable\Account\AccountType;
use Hewsda\Accountable\Account\Repository\AccountCollection;
use Hewsda\Accountable\Account\Repository\AccountTypeCollection;
use Hewsda\Accountable\Account\Service\CheckUniqueEmailAddress;
use Hewsda\Accountable\Account\UseCases\AccountSignUp;
use Hewsda\Accountable\Account\Values\AccountTypeList;

abstract class AccountSignUpHandler
{

    /**
     * @var AccountCollection
     */
    protected $accountStore;

    /**
     * @var AccountTypeCollection
     */
    protected $accountTypeStore;
    /**
     * @var CheckUniqueEmailAddress
     */
    private $checkUniqueEmailAddress;

    /**
     * AccountSignUpHandler constructor.
     *
     * @param AccountCollection $accountStore
     * @param AccountTypeCollection $accountTypeStore
     * @param CheckUniqueEmailAddress $checkUniqueEmailAddress
     */
    public function __construct(AccountCollection $accountStore,
                                AccountTypeCollection $accountTypeStore,
                                CheckUniqueEmailAddress $checkUniqueEmailAddress
    )
    {
        $this->accountStore = $accountStore;
        $this->accountTypeStore = $accountTypeStore;
        $this->checkUniqueEmailAddress = $checkUniqueEmailAddress;
    }

    public function __invoke(AccountSignUp $accountSignUp)
    {
        $account = $this->accountStore->get($accountSignUp->getAccountId());

        if ($account) {
            throw new \RuntimeException('account already exists.');
        }

        if (!($this->checkUniqueEmailAddress)($accountSignUp->getEmail())) {
            throw new \InvalidArgumentException('Account email already exists.');
        }

        $account = Account::create($accountSignUp->getAccountId(), $accountSignUp->getEmail());

        $type = $this->handle($account, $accountSignUp);
        $account->attach(AccountTypeList::asMaster($type->getIdentifier()));

        $this->accountStore->save($account);

        $this->accountTypeStore->save($type);
    }

    abstract public function handle(Account $account, AccountSignUp $command): AccountType;
}