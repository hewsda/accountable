<?php

declare(strict_types=1);

namespace Hewsda\Accountable\Account\Repository;

use Hewsda\Accountable\Account\Account;
use Hewsda\Accountable\Account\Values\AccountId;

interface AccountCollection
{
    public function get(AccountId $accountId): ?Account;

    public function save(Account $account): void;
}