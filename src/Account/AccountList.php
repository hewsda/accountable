<?php

declare(strict_types=1);

namespace Hewsda\Accountable\Account;

use Hewsda\Accountable\Account\Model\LocalUser\Values\LocalUserId;
use Hewsda\Accountable\Account\Values\AccountId;
use Hewsda\Accountable\Account\Values\AccountIdentifier;
use Hewsda\Accountable\Account\Values\AccountTypeList;
use Illuminate\Support\Collection;

class AccountList
{
    /**
     * @var Collection
     */
    private $accounts;

    /**
     * @var AccountId
     */
    private $accountId;

    /**
     * AccountList constructor.
     *
     * @param AccountId $accountId
     * @param Collection $accounts
     * @throws \InvalidArgumentException
     */
    public function __construct(AccountId $accountId, Collection $accounts)
    {
        if ($accounts->isNotEmpty()) {
            throw new \InvalidArgumentException('Account list must be constructed with an empty collection.');
        }

        $this->accountId = $accountId;

        $this->accounts = $accounts;
    }

    public function attach(AccountTypeList $typeList): AccountList
    {
        $identifier = $typeList->getIdentifier();

        if ($this->accounts->isEmpty()) {

            if (!$typeList->isMaster()) {
                throw new \InvalidArgumentException('First account must be a master account.');
            }

            $this->addAccount($typeList);

            return $this;
        }

        if ($this->accounts->contains($typeList)) {
            throw new \InvalidArgumentException('Account type already exists.');
        }

        // Only one master account per list
        if (!$this->canBeAddedAsMaster($typeList)) {
            throw new \InvalidArgumentException('Only one master account is allowed.');
        }

        // only one local account per list
        if ($this->isLocal($identifier) && !$this->canBeAdded($typeList)) {
            throw new \InvalidArgumentException('Local account can not be added.');
        }

        $this->addAccount($typeList);

        return $this;
    }

    public function detach(AccountTypeList $typeList): AccountList
    {
        if (!$this->accounts->contains($typeList)) {
            throw new \InvalidArgumentException(
                'Account type can be removed as it does not part of account list.');
        }

        if ($this->hasOnlyOneAccount()) {
            throw new \InvalidArgumentException(
                'Account can not be removed as account list contains only one account type.');
        }

        if ($typeList->isMaster()) {
            // need more operations to transform a slave into master
            throw new \InvalidArgumentException('Account master can not be removed');
        }

        if (2 === $this->accounts->count()) {
            if (!$this->isLastAccountIsMaster($typeList)) {
                throw new \InvalidArgumentException('Account can not be removed as the last account is not a master');
            }
        }

        $this->accounts->pull($typeList);

        return $this;
    }

    public function hasLocal(): bool
    {
        return $this->accounts->filter(function (AccountTypeList $typeList) {
            return $this->isLocal($typeList->getIdentifier());
        })->isNotEmpty();
    }

    public function canBeAdded(AccountTypeList $typeList): bool
    {
        if (!$this->canBeAddedAsMaster($typeList)) {
            return false;
        }

        return (!$this->hasLocal() && $this->isLocal($typeList->getIdentifier()));
    }

    public function canBeAddedAsMaster(AccountTypeList $typeList): bool
    {
        if ($typeList->isMaster()) {
            return !$this->hasMasterAccount();
        }

        return true;
    }

    public function isEmpty(): bool
    {
        return $this->accounts->isEmpty();
    }

    public function isLastAccountIsMaster(AccountTypeList $tobeRemoved): bool
    {
        if (2 !== $this->accounts->count()) {
            throw new \InvalidArgumentException('Method can only be called with a count list of two.');
        }

        return $this->accounts->reject(function (AccountTypeList $aTypeList) use ($tobeRemoved) {
            return $aTypeList->sameValueAs($tobeRemoved);
        })->first()->isMaster();
    }

    public function hasOnlyOneAccount(): bool
    {
        return 1 === $this->accounts->count();
    }

    public function hasMasterAccount(): bool
    {
        return $this->accounts->filter(function (AccountTypeList $typeList) {
            return $typeList->isMaster();
        })->isNotEmpty();
    }

    public function count(): int
    {
        return $this->accounts->count();
    }

    protected function isLocal(AccountIdentifier $identifier): bool
    {
        return $identifier->getTypeIdentifier() instanceof LocalUserId;
    }

    protected function addAccount(AccountTypeList $typeList): void
    {
        $this->accounts->push($typeList);
    }

    public function getAccountId(): AccountId
    {
        return $this->accountId;
    }
}