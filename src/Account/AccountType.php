<?php

declare(strict_types=1);

namespace Hewsda\Accountable\Account;

use Hewsda\Accountable\Account\Contracts\AccountTypeIdentifier;
use Hewsda\Accountable\Account\Contracts\AccountTypeInterface;
use Hewsda\Accountable\Account\Contracts\Entity;
use Hewsda\Accountable\Account\Values\AccountId;
use Hewsda\Accountable\Account\Values\AccountIdentifier;
use Hewsda\Accountable\Account\Values\EmailAddress;
use Hewsda\EventSourcing\AggregateRoot;

abstract class AccountType extends AggregateRoot implements AccountTypeInterface
{
    /**
     * @var AccountId
     */
    protected $accountId;

    /**
     * @var AccountTypeIdentifier
     */
    protected $typeIdentifier;

    /**
     * @var EmailAddress
     */
    protected $email;

    public function getAccountId(): AccountId
    {
        return $this->accountId;
    }

    public function getEmail(): EmailAddress
    {
        return $this->email;
    }

    public function getIdentifier(): AccountIdentifier
    {
        return AccountIdentifier::fromValues($this->getAccountId(), $this->getAccountTypeIdentifier());
    }

    public function getAccountTypeIdentifier(): AccountTypeIdentifier
    {
        return $this->typeIdentifier;
    }

    public function aggregateId(): string
    {
        return $this->typeIdentifier->toString();
    }

    public function sameIdentityAs(Entity $aEntity): bool
    {
        return $aEntity instanceof $this &&
            $this->getIdentifier()->sameValueAs($aEntity->getIdentifier());
    }
}