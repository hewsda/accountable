<?php

declare(strict_types=1);

namespace Hewsda\Accountable\Account\UseCases;

use Hewsda\Accountable\Account\Contracts\AccountTypeIdentifier;
use Hewsda\Accountable\Account\Values\AccountId;
use Hewsda\Accountable\Account\Values\AccountIdentifier;
use Hewsda\Accountable\Account\Values\EmailAddress;
use Prooph\Common\Messaging\Command;
use Prooph\Common\Messaging\PayloadConstructable;
use Prooph\Common\Messaging\PayloadTrait;

abstract class AccountSignUp extends Command implements PayloadConstructable
{
    use PayloadTrait;

    public function getAccountId(): AccountId
    {
        return AccountId::fromString($this->payload['account_id']);
    }

    public function getIdentifier(): AccountIdentifier
    {
        return AccountIdentifier::fromValues($this->getAccountId(), $this->getAccountTypeIdentifier());
    }

    public function getEmail(): EmailAddress
    {
        return EmailAddress::fromString($this->payload['email']);
    }

    abstract public function getAccountTypeIdentifier(): AccountTypeIdentifier;
}