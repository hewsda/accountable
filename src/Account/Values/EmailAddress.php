<?php

declare(strict_types=1);

namespace Hewsda\Accountable\Account\Values;

use Hewsda\Accountable\Account\Contracts\Value;
use Hewsda\Accountable\Application\Exceptions\Assertion;

class EmailAddress implements Value
{
    /**
     * @var string
     */
    private $email;

    /**
     * EmailAddress constructor.
     *
     * @param string $email
     */
    private function __construct(string $email)
    {
        $this->email = $email;
    }

    public static function fromString($email): self
    {
        Assertion::email($email);

        return new self($email);
    }

    public function sameValueAs(Value $aValue): bool
    {
        return $aValue instanceof $this && $this->getEmail() === $aValue->getEmail();
    }

    public function getEmail(): string
    {
        return $this->toString();
    }

    public function toString(): string
    {
        return $this->email;
    }

    public function __toString(): string
    {
        return $this->toString();
    }
}