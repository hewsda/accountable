<?php

declare(strict_types=1);

namespace Hewsda\Accountable\Account\Model\Values;

use Hewsda\Accountable\Account\Contracts\Value;
use Hewsda\Accountable\Application\Exceptions\Assertion;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class UserId implements Value
{
    /**
     * @var UuidInterface
     */
    private $uid;

    /**
     * UserId constructor.
     *
     * @param UuidInterface $uid
     */
    private function __construct(UuidInterface $uid)
    {
        $this->uid = $uid;
    }

    public function nextIdentity(): self
    {
        return new self(Uuid::uuid4());
    }

    public static function fromString($userId): self
    {
        Assertion::string($userId);
        Assertion::uuid($userId);

        return new self(Uuid::fromString($userId));
    }

    public function sameValueAs(Value $aValue): bool
    {
        return $aValue instanceof $this;
    }

    public function toString(): string
    {
        return $this->uid->toString();
    }

    public function __toString(): string
    {
        return $this->toString();
    }
}