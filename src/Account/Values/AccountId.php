<?php

declare(strict_types=1);

namespace Hewsda\Accountable\Account\Values;

use Hewsda\Accountable\Account\Contracts\Value;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class AccountId implements Value
{
    private $uid;

    private function __construct(UuidInterface $uuid)
    {
        $this->uid = $uuid;
    }

    public static function nextIdentity(): self
    {
        return new self(Uuid::uuid4());
    }

    public static function fromString($accountId): self
    {
        return new self(Uuid::fromString($accountId));
    }

    public function toString(): string
    {
        return $this->uid->toString();
    }

    public function sameValueAs(Value $aValue): bool
    {
        return $aValue instanceof $this && $this->toString() === $aValue->toString();

    }
}