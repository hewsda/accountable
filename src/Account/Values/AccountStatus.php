<?php

declare(strict_types=1);

namespace Hewsda\Accountable\Account\Values;

use Hewsda\Accountable\Account\Contracts\Value;
use Hewsda\Accountable\Application\Exceptions\Assertion;
use Hewsda\Accountable\Application\Exceptions\ValidationException;
use Illuminate\Contracts\Support\Arrayable;

class AccountStatus implements Value, Arrayable
{
    const STATUS_DEFAULT = 0;
    const STATUS_AWAITING_CONFIRMATION = 101;
    const STATUS_ACTIVATED = 200;
    const STATUS_DISABLED = 400;
    const STATUS_MANUAL_OPERATION = 401;
    const STATUS_SUSPENDED = 410;
    const STATUS_BANNED = 410;
    const STATUS_CREDENTIALS_EXPIRED = 422;

    /**
     * @var array
     */
    private $statuses = [
        0 => 'User Pending Registration',
        101 => 'User Awaiting Confirmation',
        200 => 'User Activated',
        400 => 'User Disabled',
        401 => 'Manual Operation Required',
        410 => 'User Suspended',
        411 => 'User Banned',
        422 => 'User Credentials Expired'
    ];

    /**
     * @var int
     */
    private $code;

    /**
     * UserStatus constructor.
     *
     * @param int $code
     */
    private function __construct(int $code)
    {
        $this->code = $code;
    }

    public static function fromCode($code): self
    {
        Assertion::integer($code);

        $self = new static($code);
        $self->messageFor($code);

        return $self;
    }

    public function isEnabled(): bool
    {
        return $this->code >= 200 && $this->code <= 399;
    }

    public function isDisabled(): bool
    {
        return $this->code >= 400 && $this->code <= 499;
    }

    public function isActivated(): bool
    {
        return 200 === $this->code;
    }

    public function isAwaiting(): bool
    {
        return 101 === $this->code;
    }

    public function messageFor(int $code): string
    {
        if (array_key_exists($code, $this->statuses)) {
            return $this->statuses[$code];
        }

        throw new ValidationException(
            'Status does not exists.',
            Assertion::INVALID_SATISFY,
            AccountStatus::class,
            $code);
    }

    public function sameValueAs(Value $aValue): bool
    {
        return $aValue instanceof $this &&
            $this->code === $aValue->getCode();
    }

    public function toArray()
    {
        return ['code' => $this->code, 'message' => $this->messageFor($this->code)];
    }

    public function getMessage(): string
    {
        return $this->messageFor($this->code);
    }

    public function getCode(): int
    {
        return $this->code;
    }

    public function getStatuses(): array
    {
        return $this->statuses;
    }
}