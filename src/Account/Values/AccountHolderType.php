<?php

declare(strict_types=1);

namespace Hewsda\Accountable\Account\Values;

use Hewsda\Accountable\Account\Contracts\Value;
use Hewsda\Accountable\Account\Model\LocalUser\Values\LocalUserId;
use Ramsey\Uuid\UuidInterface;

abstract class AccountHolderType implements Value
{
    /**
     * @var UuidInterface
     */
    protected $uuid;

    /**
     * @var AccountIdentifier
     */
    protected $accountIdentifier;

    /**
     * AccountMaster constructor.
     *
     * @param UuidInterface $uuid
     * @param AccountIdentifier $accountIdentifier
     */
    protected function __construct(UuidInterface $uuid, AccountIdentifier $accountIdentifier)
    {
        $this->uuid = $uuid;
        $this->accountIdentifier = $accountIdentifier;
    }

    public function getUuid(): UuidInterface
    {
        return $this->uuid;
    }

    public function getAccountIdentifier(): AccountIdentifier
    {
        return $this->accountIdentifier;
    }

    // todo make a vo
    public function isLocal(): bool
    {
        return $this->accountIdentifier->getTypeIdentifier() instanceof LocalUserId;
    }
}