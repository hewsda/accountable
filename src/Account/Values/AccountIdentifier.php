<?php

declare(strict_types=1);

namespace Hewsda\Accountable\Account\Values;

use Hewsda\Accountable\Account\Contracts\AccountTypeIdentifier;
use Hewsda\Accountable\Account\Contracts\Value;

class AccountIdentifier implements Value
{
    /**
     * @var AccountId
     */
    private $accountId;

    /**
     * @var AccountTypeIdentifier
     */
    private $typeIdentifier;

    /**
     * AccountIdentifier constructor.
     *
     * @param AccountId $accountId
     * @param AccountTypeIdentifier $typeIdentifier
     */
    private function __construct(AccountId $accountId, AccountTypeIdentifier $typeIdentifier)
    {
        $this->accountId = $accountId;
        $this->typeIdentifier = $typeIdentifier;
    }

    public static function fromValues(AccountId $accountId, AccountTypeIdentifier $typeIdentifier): self
    {
        return new self($accountId, $typeIdentifier);
    }

    public function sameValueAs(Value $aValue): bool
    {
        return $aValue instanceof $this &&
            $this->accountId->sameValueAs($aValue->getAccountId()) &&
            $this->typeIdentifier->sameValueAs($aValue->getTypeIdentifier());
    }

    public function getAccountId(): AccountId
    {
        return $this->accountId;
    }

    public function getTypeIdentifier(): AccountTypeIdentifier
    {
        return $this->typeIdentifier;
    }
}