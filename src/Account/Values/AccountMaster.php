<?php

declare(strict_types=1);

namespace Hewsda\Accountable\Account\Values;

use Hewsda\Accountable\Account\Contracts\Value;
use Hewsda\Accountable\Application\Exceptions\Assertion;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class AccountMaster extends AccountHolderType
{
    public static function fromValues(AccountIdentifier $identifier, UuidInterface $uuid): self
    {
        Assertion::eq($uuid->getVersion(), 4, 'Wrong type of Uuid version.');

        return new self($uuid, $identifier);
    }

    public static function fromAccountIdentifier(AccountIdentifier $identifier): self
    {
        return new self(Uuid::uuid4(), $identifier);
    }

    public function sameValueAs(Value $aValue): bool
    {
        return $aValue instanceof $this &&
            $this->uuid->equals($aValue->getUuid()) &&
            $this->accountIdentifier->sameValueAs($aValue->getAccountIdentifier());
    }
}