<?php

declare(strict_types=1);

namespace Hewsda\Accountable\Account\Event;

use Hewsda\Accountable\Account\Contracts\AccountTypeIdentifier;
use Hewsda\Accountable\Account\Values\AccountId;
use Hewsda\EventSourcing\AggregateChanged;

class AccountMasterTypeWasAttached extends AggregateChanged
{
    private $accountId;
    private $typeIdentifier;
    private $typeIdentifierClass;
    private $type;

    public static function withType(AccountId $accountId, AccountTypeIdentifier $typeIdentifier, string $type): self
    {
        $self = self::occur($accountId->toString(), [
            'type_identifier' => $typeIdentifier->toString(),
            'type_identifier_class' => get_class($typeIdentifier),
            'type' => $type
        ]);

        $self->accountId = $accountId;
        $self->typeIdentifier = $typeIdentifier;
        $self->typeIdentifierClass = get_class($typeIdentifier);
        $self->type = $type;

        return $self;
    }

    public function getAccountId(): AccountId
    {
        return $this->accountId ?? AccountId::fromString($this->payload['account_id']);
    }

    public function getTypeIdentifier(): AccountTypeIdentifier
    {
        if ($this->typeIdentifier) {
            return $this->typeIdentifier;
        }

        $class = $this->typeIdentifierClass ?? $this->payload['type_identifier_class'];

        if (is_callable([$class, 'fromIdentifier'])) {
            return $class::fromIdentifier($this->payload['type_identifier']);
        }

        throw new \InvalidArgumentException('Identifier type can not be accessed.');
    }

    public function getType(): string
    {
        return $this->type ?? $this->payload['type'];
    }
}