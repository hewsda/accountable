<?php

declare(strict_types=1);

namespace Hewsda\Accountable\Account\Event;

use Hewsda\Accountable\Account\Model\LocalUser\Values\LocalUserId;
use Hewsda\Accountable\Account\Values\AccountId;
use Hewsda\Accountable\Account\Values\AccountIdentifier;
use Hewsda\Accountable\Account\Values\AccountMaster;
use Hewsda\Accountable\Account\Values\EmailAddress;
use Hewsda\EventSourcing\AggregateChanged;

class AccountWasCreated extends AggregateChanged
{
    public static function withData(AccountMaster $accountMaster, EmailAddress $email)
    {
        $self = self::occur($accountMaster->getAccountIdentifier()->getAccountId()->toString(), [
            'type_identifier' => $accountMaster->getAccountIdentifier()->getTypeIdentifier()->toString(),
            'email' => $email->toString()
        ]);

        return $self;
    }

    public function getAccountMaster(): AccountMaster
    {
        return AccountMaster::fromAccountIdentifier(
            AccountIdentifier::fromValues(
                AccountId::fromString($this->aggregateId()),
                LocalUserId::fromString($this->payload['type_identifier'])
            )
        );
    }

    public function getEmailAddress(): EmailAddress
    {
        return EmailAddress::fromString($this->payload['email']);
    }
}