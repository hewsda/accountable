<?php

declare(strict_types=1);

namespace Hewsda\Accountable\Account;

use Hewsda\Accountable\Account\Contracts\AccountInterface;
use Hewsda\Accountable\Account\Contracts\Entity;
use Hewsda\Accountable\Account\Event\AccountWasCreated;
use Hewsda\Accountable\Account\Event\LocalAccountWasCreated;
use Hewsda\Accountable\Account\Model\LocalUser\LocalUser;
use Hewsda\Accountable\Account\Model\LocalUser\Service\LocalPasswordChanger;
use Hewsda\Accountable\Account\Model\LocalUser\Values\EncodedPassword;
use Hewsda\Accountable\Account\Values\AccountId;
use Hewsda\Accountable\Account\Values\AccountIdentifier;
use Hewsda\Accountable\Account\Values\AccountMaster;
use Hewsda\Accountable\Account\Values\EmailAddress;
use Hewsda\EventSourcing\AggregateRoot;

class Account extends AggregateRoot implements AccountInterface
{
    /**
     * @var AccountId
     */
    private $accountId;

    /**
     * @var EmailAddress
     */
    private $email;

    /**
     * @var
     */
    private $status;

    /**
     * @var AccountMaster
     */
    private $master;

    /**
     * @var AccountList
     */
    private $list;

    public static function createLocalUser(AccountIdentifier $identifier, EmailAddress $emailAddress, EncodedPassword $encodedPassword)
    {
        // creation is always a master account type

        $self = new self();

        $self->publish(
            AccountWasCreated::withData(
                AccountMaster::fromAccountIdentifier($identifier),
                $emailAddress
            ));

        $self->publish(
            LocalAccountWasCreated::withData(
                AccountMaster::fromAccountIdentifier($identifier),
                $emailAddress,
                $encodedPassword
            ));

        return $self;
    }

    public function changeLocalPassword(LocalUser $localUser, LocalPasswordChanger $passwordChanger): ?LocalUser
    {

        if(! $this->list->hasLocal()){
            throw new \InvalidArgumentException('No local user');
        }

        // check status


        return $localUser->changePassword($passwordChanger);
    }

    protected function whenAccountWasCreated(AccountWasCreated $event)
    {
        $this->accountId = $event->getAccountMaster()->getAccountIdentifier()->getAccountId();
        $this->email = $event->getEmailAddress();
        $this->status = 0;

        // list status roles
    }

    protected function whenLocalAccountWasCreated(LocalAccountWasCreated $event)
    {
        $this->master = $event->getAccountMaster();
        $this->list = new AccountHolder($this->master);
    }

    public function getAccountId(): AccountId
    {
        return $this->accountId;
    }

    public function getEmail(): EmailAddress
    {
        return $this->email;
    }

    public function sameIdentityAs(Entity $aEntity): bool
    {
        return $aEntity instanceof $this &&
            $this->accountId->sameValueAs($aEntity->getAccountId());
    }

    public function aggregateId(): string
    {
        return $this->accountId->toString();
    }

    public function getMaster(): AccountMaster
    {
        return $this->master;
    }
}