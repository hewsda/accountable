<?php

declare(strict_types=1);

namespace Hewsda\Accountable\NotAEventStore\Adapter;

use Hewsda\Accountable\NotAEventStore\Stream\Stream;
use Hewsda\Accountable\NotAEventStore\Stream\StreamName;

interface Adapter
{
    public function create(Stream $stream): void;

    public function load(StreamName $streamName): ?Stream;
}