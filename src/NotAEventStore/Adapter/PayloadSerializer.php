<?php

declare(strict_types=1);

namespace Hewsda\Accountable\NotAEventStore\Adapter;

interface PayloadSerializer
{
    public function serializePayload(array $payload): string;

    public function unserializePayload(string $serialized): array;
}