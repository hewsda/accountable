<?php

declare(strict_types=1);

namespace Hewsda\Accountable\NotAEventStore\Adapter\PayloadSerializer;

use Hewsda\Accountable\NotAEventStore\Adapter\PayloadSerializer;

class JsonPayloadSerializer implements PayloadSerializer
{

    public function serializePayload(array $payload): string
    {
        return json_encode($payload);
    }

    public function unserializePayload(string $serialized): array
    {
        return json_decode($serialized);
    }
}