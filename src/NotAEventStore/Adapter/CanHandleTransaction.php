<?php

declare(strict_types=1);

namespace Hewsda\Accountable\NotAEventStore\Adapter;

interface CanHandleTransaction
{
    public function beginTransaction(): void;
    
    public function commit(): void;
    
    public function rollback(): void;
}