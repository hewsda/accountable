<?php

declare(strict_types=1);

namespace Hewsda\Accountable\NotAEventStore\Plugin;

use Hewsda\Accountable\NotAEventStore\EventStore;

interface Plugin
{
    public function setUp(EventStore $eventStore): void;
}