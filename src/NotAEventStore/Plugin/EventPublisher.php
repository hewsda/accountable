<?php

declare(strict_types=1);

namespace Hewsda\Accountable\NotAEventStore\Plugin;

use Hewsda\Accountable\NotAEventStore\EventStore;
use Prooph\Common\Event\ActionEvent;
use Prooph\ServiceBus\EventBus;

class EventPublisher implements Plugin
{
    /**
     * @var EventBus
     */
    private $eventBus;

    /**
     * EventPublisher constructor.
     *
     * @param EventBus $eventBus
     */
    public function __construct(EventBus $eventBus)
    {
        $this->eventBus = $eventBus;
    }

    public function setUp(EventStore $eventStore): void
    {
        $eventStore->getEmitter()->attachListener('commit.post', [$this, 'onEventStoreCommitPost']);
    }

    public function onEventStoreCommitPost(ActionEvent $actionEvent): void
    {
        /** @var array $recordedEvents */
        $recordedEvents = $actionEvent->getParam('recordedEvents', new \ArrayIterator());

        foreach ($recordedEvents as $recordedEvent) {
            $this->eventBus->dispatch($recordedEvent);
        }
    }
}