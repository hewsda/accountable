<?php

declare(strict_types=1);

namespace Hewsda\Accountable\NotAEventStore\Exception;

class InvalidArgumentException extends \InvalidArgumentException implements EventStoreException
{

}