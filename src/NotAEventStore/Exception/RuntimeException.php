<?php

declare(strict_types=1);

namespace Hewsda\Accountable\NotAEventStore\Exception;

class RuntimeException extends \RuntimeException implements EventStoreException
{

}