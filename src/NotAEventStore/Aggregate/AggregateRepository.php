<?php

declare(strict_types=1);

namespace Hewsda\Accountable\NotAEventStore\Aggregate;

use Hewsda\Accountable\NotAEventStore\EventStore;
use Hewsda\Accountable\NotAEventStore\Exception\EventStoreException;
use Hewsda\Accountable\NotAEventStore\Stream\Stream;
use Hewsda\Accountable\NotAEventStore\Stream\StreamName;

class AggregateRepository
{
    /**
     * @var array
     */
    protected $identityMap = [];

    /**
     * @var EventStore
     */
    protected $eventStore;

    /**
     * @var AggregateType
     */
    protected $aggregateType;

    /**
     * AggregateRepository constructor.
     *
     * @param EventStore $eventStore
     * @param AggregateType $aggregateType
     */
    public function __construct(EventStore $eventStore, AggregateType $aggregateType)
    {
        $this->eventStore = $eventStore;
        $this->aggregateType = $aggregateType;
    }

    public function addAggregateRoot($eventSourcedAggregateRoot): void
    {
        $this->assertAggregateType($eventSourcedAggregateRoot);

        $domainEvents = $eventSourcedAggregateRoot->popRecordedEvents();
        $aggregateId = $eventSourcedAggregateRoot->aggregateId();

        $stream = new Stream($this->determineEventStreamName($aggregateId), new \ArrayIterator($domainEvents));

        $this->eventStore->create($stream);
    }

    public function getAggregateRoot(string $aggregateId)
    {
        if (isset($this->identityMap[$aggregateId])) {
            return $this->identityMap[$aggregateId];
        }

        $streamName = $this->determineEventStreamName($aggregateId);

        // fixMe rid off try/catch
        try {
            $sourced = $this->eventStore->load($streamName)->streamEvents();

        } catch (EventStoreException $exception) {
            return null;
        }

        $sourcedAg = $this->reconstituteFromHistory($sourced);

        $this->identityMap[$aggregateId] = $sourcedAg;

        return $sourcedAg;
    }

    // todo translator
    protected function reconstituteFromHistory(\Iterator $events)
    {
        $aggregateClass = $this->aggregateType->toString();

        $sourcedAg = $aggregateClass::reconstituteFromHistory($events);

        if (!$sourcedAg instanceof $aggregateClass) {
            throw new \RuntimeException('Reconstitute from history failed.');
        }

        return $sourcedAg;
    }

    protected function determineEventStreamName(string $aggregateId): StreamName
    {
        return new StreamName($this->aggregateType->toString() . '-' . $aggregateId);
    }

    protected function assertAggregateType($eventSourcedAggregateRoot): void
    {
        $this->aggregateType->assert($eventSourcedAggregateRoot);
    }
}