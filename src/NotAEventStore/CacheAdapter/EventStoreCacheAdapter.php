<?php

declare(strict_types=1);

namespace Hewsda\Accountable\NotAEventStore\CacheAdapter;

use Hewsda\Accountable\NotAEventStore\Adapter\Adapter;
use Hewsda\Accountable\NotAEventStore\Adapter\CanHandleTransaction;
use Hewsda\Accountable\NotAEventStore\Stream\Stream;
use Hewsda\Accountable\NotAEventStore\Stream\StreamName;
use Illuminate\Contracts\Cache\Repository;

class EventStoreCacheAdapter implements Adapter, CanHandleTransaction
{
    /**
     * @var [\Iterator]
     */
    private $streams = [];

    /**
     * @var Repository
     */
    private $store;

    /**
     * EventStoreCacheAdapter constructor.
     *
     * @param Repository $store
     */
    public function __construct(Repository $store)
    {
        $this->store = $store;
    }

    public function create(Stream $stream): void
    {
        $streamEvents = $stream->streamEvents();

        $streamEvents->rewind();

        $this->streams[$stream->streamName()->toString()] = $streamEvents;
    }

    public function load(StreamName $streamName): ?Stream
    {
        if ($this->store->has($streamName->toString())) {
            $streamEvents = $this->store->get($streamName->toString());

            $streamEvents->rewind();

            return new Stream($streamName, $streamEvents);
        }

        return null;
    }

    public function beginTransaction(): void
    {
    }

    public function commit(): void
    {
        if (!$this->streams) {
            return;
        }

        $streamName = key($this->streams);

        /** @var \ArrayIterator $events */
        $events = $this->store->get($streamName);

        if (!$events) {
            $this->store->forever($streamName, $this->streams[$streamName]);

            $this->streams = [];

            return;
        }

        $events->append(current($this->streams[$streamName]));

        $this->store->forever(key($this->streams), $events);

        $this->streams = [];
    }

    public function rollback(): void
    {
        /*
        if ($this->streams) {
            $this->store->forget(key($this->streams));
        }
        */

        $this->streams = [];
    }
}