<?php

declare(strict_types=1);

namespace Hewsda\Accountable\Infrastructure\Repository;

use Hewsda\Accountable\Account\Model\LocalUser\LocalUser;
use Hewsda\Accountable\Account\Model\LocalUser\Repository\LocalAccountCollection;
use Hewsda\Accountable\Account\Model\LocalUser\Values\LocalUserId;
use Hewsda\Accountable\NotAEventStore\Aggregate\AggregateRepository;
use Hewsda\Accountable\NotAEventStore\EventStore;

class EventStoreLocalAccount extends AggregateRepository implements LocalAccountCollection
{

    public function get(LocalUserId $localUserId): ?LocalUser
    {
        return $this->getAggregateRoot($localUserId->toString());
    }

    public function save(LocalUser $localUser): void
    {
        $this->eventStore->transactional(function (EventStore $eventStore) use ($localUser) {
            $this->addAggregateRoot($localUser);
        });
    }
}