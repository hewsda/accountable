<?php

declare(strict_types=1);

namespace Hewsda\Accountable\Infrastructure\Repository;

use Hewsda\Accountable\Account\Account;
use Hewsda\Accountable\Account\Repository\AccountCollection;
use Hewsda\Accountable\Account\Values\AccountId;
use Hewsda\Accountable\NotAEventStore\Aggregate\AggregateRepository;
use Hewsda\Accountable\NotAEventStore\EventStore;

class EventStoreAccount extends AggregateRepository implements AccountCollection
{

    public function get(AccountId $accountId): ?Account
    {
        return $this->getAggregateRoot($accountId->toString());
    }

    public function save(Account $account): void
    {
        $this->eventStore->transactional(function (EventStore $eventStore) use ($account) {
            $eventStore->beginTransaction();

            $this->addAggregateRoot($account);
        });
    }
}