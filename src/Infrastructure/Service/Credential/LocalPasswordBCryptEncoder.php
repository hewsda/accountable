<?php

declare(strict_types=1);

namespace Hewsda\Accountable\Infrastructure\Service\Credential;

use Hewsda\Accountable\Account\Model\LocalUser\Service\EncoderPasswordService;
use Hewsda\Accountable\Account\Model\LocalUser\Values\BCryptEncodedPassword;
use Hewsda\Accountable\Account\Model\LocalUser\Values\EncodedPassword;
use Hewsda\Accountable\Account\Model\LocalUser\Values\Password;
use Illuminate\Contracts\Hashing\Hasher;

class LocalPasswordBCryptEncoder implements EncoderPasswordService
{
    /**
     * @var Hasher
     */
    private $encoder;

    /**
     * LocalPasswordBCryptEncoder constructor.
     *
     * @param Hasher $encoder
     */
    public function __construct(Hasher $encoder)
    {
        $this->encoder = $encoder;
    }

    public function __invoke(Password $password): EncodedPassword
    {
        return BCryptEncodedPassword::fromEncodedPassword(
            $this->encoder->make($password->toString())
        );
    }
}