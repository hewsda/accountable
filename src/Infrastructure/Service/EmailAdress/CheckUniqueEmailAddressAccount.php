<?php

declare(strict_types=1);

namespace Hewsda\Accountable\Infrastructure\Service\EmailAdress;

use Hewsda\Accountable\Account\Service\CheckUniqueEmailAddress;
use Hewsda\Accountable\Account\Values\EmailAddress;
use Hewsda\Accountable\Projection\Account\AccountFinder;

class CheckUniqueEmailAddressAccount implements CheckUniqueEmailAddress
{
    /**
     * @var AccountFinder
     */
    private $accountFinder;

    /**
     * CheckUniqueEmailAddressAccount constructor.
     *
     * @param AccountFinder $accountFinder
     */
    public function __construct(AccountFinder $accountFinder)
    {
        $this->accountFinder = $accountFinder;
    }

    public function __invoke(EmailAddress $emailAddress): bool
    {
        return true;
    }
}