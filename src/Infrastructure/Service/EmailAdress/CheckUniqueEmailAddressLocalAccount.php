<?php

declare(strict_types=1);

namespace Hewsda\Accountable\Infrastructure\Service\EmailAdress;

use Hewsda\Accountable\Account\Model\LocalUser\Service\CheckLocalUniqueEmailAddress;
use Hewsda\Accountable\Account\Values\EmailAddress;
use Hewsda\Accountable\Projection\Account\LocalUserFinder;

class CheckUniqueEmailAddressLocalAccount implements CheckLocalUniqueEmailAddress
{
    /**
     * @var LocalUserFinder
     */
    private $finder;

    /**
     * CheckUniqueEmailAddressLocalAccount constructor.
     *
     * @param LocalUserFinder $finder
     */
    public function __construct(LocalUserFinder $finder)
    {
        $this->finder = $finder;
    }

    public function invokeForAccountType(EmailAddress $emailAddress): bool
    {
        return true;
    }

    public function __invoke(EmailAddress $emailAddress): bool
    {
        return $this->invokeForAccountType($emailAddress);
    }
}